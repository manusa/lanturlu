extends KinematicBody

const MOVEMENTINTERPOLATION = 15.0
const ROTATIONINTERPOLATION = 10.0

onready var anim = get_node("AnimationPlayer")
onready var cam = get_node("HeadCam")

# Declare member variables here. Examples:
var dir = Vector3()
var motion = Vector2()
var speed = 600

var basis = Basis()
var orientation = Transform()

var isMovingCamera = false
var characterMesh
var cameraBase
var cameraPivot

func _init():
	pass
	#Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

# Called when the node enters the scene tree for the first time.
func _ready():
	#Gets all the needed charcter related variables
	characterMesh = get_node("HumanArmature")
	orientation = characterMesh.global_transform
	orientation.origin = Vector3()
	#Gets all the needed camera related variables
	basis = global_transform.basis


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	motion = Vector2(Input.get_action_strength("ui_left") - Input.get_action_strength("ui_right"),
					 Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up"))

	# Quits the game
	if Input.is_action_just_pressed("ui_cancel"): get_tree().quit()

	dir = Vector3(0,0,0)
	if Input.is_action_pressed("ui_up"):
		dir.z += 1
	if Input.is_action_pressed("ui_down"):
		dir.z -= 1
	if Input.is_action_pressed("ui_right"):
		dir.x -= 1
	if Input.is_action_pressed("ui_left"):
		dir.x += 1

	if Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down") or Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left"):
		anim.play('Run')
	elif Input.is_action_pressed("ui_select"):
		anim.play('Punch')
	else:
		anim.play('Idle')
	
	dir.normalized()
	dir *= delta*speed
	if motion.length() > 0.01:
		# Rotates the charcter to the direction movement if any
		rotateCharacter(-dir, delta)

	var _collision = move_and_slide(dir, Vector3(0,1,0))
	global_transform.basis = basis
	
func rotateCharacter(direction, delta):
	var q_from = Quat(orientation.basis)
	var q_to = Quat(Transform().looking_at(direction,Vector3.UP).basis)
	
	# Interpolate current rotation with desired one
	orientation.basis = Basis(q_from.slerp(q_to, delta * ROTATIONINTERPOLATION))
	
	orientation = orientation.orthonormalized() # orthonormalize orientation
	characterMesh.global_transform.basis = orientation.basis
