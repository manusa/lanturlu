extends KinematicBody

const MOVEMENTINTERPOLATION = 15.0
const ROTATIONINTERPOLATION = 10.0

onready var anim = get_node("AnimationPlayer")
onready var head_cam = get_node("HeadCam")
onready var bird_cam = get_node("BirdCam")
onready var skel = get_node("lp_guy/Skeleton")

# Declare member variables here. Examples:
var current_cam
var dir = Vector2()
var motion = Vector3()
var speed = 16
var accel = 16
var grav = -90

var basis = Basis()
var orientation = Transform()

var isMovingCamera = false
var characterMesh
var headbone
var initial_head_transform
var cameraBase
var cameraPivot
var mouse_scroll_value = 100
var mouse_scroll_value_MIN = 50
var mouse_scroll_value_MAX = 150

func _init():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event):
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if event is InputEventMouseMotion:
			var move = event.relative
			head_cam.rotation.x += -move.y/100
			head_cam.rotation.x = clamp(head_cam.rotation.x,-0.9,0.9)
			bird_cam.rotation.x = head_cam.rotation.x
			rotation.y += -move.x/100
		if event is InputEventMouseButton:
			if event.button_index == BUTTON_WHEEL_UP or event.button_index == BUTTON_WHEEL_DOWN:
				if event.button_index == BUTTON_WHEEL_UP:
					mouse_scroll_value -= 1
				elif event.button_index == BUTTON_WHEEL_DOWN:
					mouse_scroll_value += 1
				mouse_scroll_value = clamp(mouse_scroll_value, mouse_scroll_value_MIN, mouse_scroll_value_MAX)
				current_cam.fov = mouse_scroll_value

# Called when the node enters the scene tree for the first time.
func _ready():
	# Gets all the needed charcter related variables
	characterMesh = $lp_guy
	orientation = characterMesh.global_transform
	orientation.origin = Vector3()
	headbone = skel.find_bone("head")
	head_cam.translation = skel.get_bone_global_pose(headbone).origin
	initial_head_transform = skel.get_bone_pose(headbone)
	current_cam = head_cam
	# Gets all the needed camera related variables
	basis = global_transform.basis
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_switch_cam"):
		if head_cam.current:
			head_cam.current = false
			bird_cam.current = true
			current_cam = bird_cam
		else:
			head_cam.current = true
			bird_cam.current = false
			current_cam = head_cam

	dir = Vector2(0,0)
	if Input.is_action_pressed("ui_up"):
		dir.y += 1
	if Input.is_action_pressed("ui_down"):
		dir.y -= 1
	if Input.is_action_pressed("ui_right"):
		dir.x -= 1
	if Input.is_action_pressed("ui_left"):
		dir.x += 1

	if Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down") or Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left"):
		anim.play('Run')
	elif Input.is_action_pressed("ui_select"):
		anim.play('Attack_1')
	else:
		anim.play('Idle-loop')

	dir = dir.normalized().rotated(-rotation.y)
	motion.x = lerp(motion.x, dir.x * speed, delta * accel)
	motion.z = lerp(motion.z, dir.y * speed, delta * accel)
	motion.y = grav * delta
	
	if dir != Vector2(0,0):
		# Rotates the charcter to the direction movement if any
		rotateCharacter(-motion, delta)
	
	var _collision = move_and_slide(motion, Vector3.UP)
	if is_on_floor() and motion.y < 0:
		motion.y = 0
	
	var current_head_transform = initial_head_transform.rotated(Vector3(-1, 0, 0), head_cam.rotation.x)
	skel.set_bone_pose(headbone, current_head_transform)
	if head_cam.current:
		head_cam.translation = skel.get_bone_global_pose(headbone).origin+Vector3(0,0,0.1)
	#global_transform.basis = basis
	
func rotateCharacter(direction, delta):
	var q_from = Quat(orientation.basis)
	var q_to = Quat(Transform().looking_at(direction,Vector3.UP).basis)
	
	# Interpolate current rotation with desired one
	orientation.basis = Basis(q_from.slerp(q_to, delta * ROTATIONINTERPOLATION))
	
	orientation = orientation.orthonormalized() # orthonormalize orientation
	characterMesh.global_transform.basis = orientation.basis


