extends DirectionalLight

func set_sun_pos(day_of_year, time_of_day, coord):
	# fractional year 
	var t = 2 * PI / 365 * (day_of_year + time_of_day - 1.5)
	var eqtime = 229.18 * (0.000075 + 0.001868 * cos(t) - 0.032077*sin(t)-0.014615*cos(2*t)-0.040849*sin(2*t))
	var decl = 0.006918 - 0.399912 * cos(t) + 0.070257 * sin(t) - 0.006758 * cos(2*t) + 0.000907 * sin(2*t) - 0.002697 * cos(3*t) + 0.00148 * sin(3*t)
	var offset = 0
	var time_offset = eqtime - 4*coord.y + 60*offset
	# Solar hour angle
	var ha = time_offset/4 - 180
	var phi = acos(sin(coord.x)*sin(decl)+cos(coord.x)*cos(decl)*cos(t))
	var theta = PI + acos((sin(coord.x)*cos(phi)-sin(decl))/(cos(coord.x)*sin(phi)))
	rotation.x = - theta
	rotation.y = phi
	# print_debug(str(phi) + " " + str(theta))
	
	# TODO
	phi = time_of_day * 2 * PI
	var sun_pos = Vector3(0,-1,0).rotated(Vector3(1,0,0),phi).rotated(Vector3(0,0,1),0.2)
	look_at_from_position(sun_pos, Vector3.ZERO, Vector3.UP)
