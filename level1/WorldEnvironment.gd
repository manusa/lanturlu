extends WorldEnvironment

var time_of_day: float=0.5
var day_of_year: float=0.0
# lat, long, elev
var coords: Vector3 = Vector3(PI/2, 0, 0)
var counter: float = 0;
var update_every: int = 10; # seconds
export(float,1,1000,1) var speed: float = 1

func _ready():
	get_node("Sun").set_sun_pos(day_of_year, time_of_day, coords)
	print_date()


func _process(delta):
	counter += delta * speed
	if counter > update_every:
		time_of_day += counter / (3600.0 * 24.0)
		if time_of_day >= 1:
			day_of_year += int(time_of_day)
			time_of_day -= int(time_of_day)
		print_date()
		get_node("Sun").set_sun_pos(day_of_year, time_of_day, coords)
		counter = 0

func set_lattitude(val):
	coords.x = val
	get_node("Sun").set_sun_pos(day_of_year, time_of_day, coords)

func print_date():
	get_node("../Control/Panel/Heure").text = "Heure : " + str(day_of_year) + str("  ") + str(time_of_day)

func _on_TimeFaster_pressed() -> void:
	speed *= 2
	print_debug("Speed is ", speed)

func _on_TimeReset_pressed() -> void:
	speed = 1
